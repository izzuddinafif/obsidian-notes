#gitlab #ci/cd

[YouTube Video](https://www.youtube.com/watch?v=8aV5AxJrHDg&t=228s&ab_channel=LambdaTest)

# Git basics refresher
I will only list commands that are new to me here.
```sh
git init reponame # initializing a repo under certain dir name
git config --global init.defaultBranch main # set default branch name as main
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
git restore filename # discard changes
```

